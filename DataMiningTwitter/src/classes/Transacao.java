/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class Transacao {

    private List<String> palavra;
    private int idtransacao;
    private String idtweet;
    private String autor;
    
    public Transacao()
    {
        palavra = new ArrayList<String>(); 
    }
    
    public boolean Salvar() {
        Connection c = ConexaoBD.getconexao();
        
        String ConsultaSQL = "";

        ConsultaSQL = "INSERT INTO `dataminingtwitter`.`transacao` (`idtweet`,`autor`) VALUES (?, ?)";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setString(1, idtweet);
            Query1.setString(2, autor);
            Query1.execute();

            ConsultaSQL = "SELECT MAX(idtransacao) AS id FROM `dataminingtwitter`.`transacao`";
            PreparedStatement Query2 = c.prepareStatement(ConsultaSQL);
            rs = Query2.executeQuery();
            if (rs.next()) {
                for (String pl : palavra) {
                    ConsultaSQL = "INSERT INTO `dataminingtwitter`.`palavras` (`idtransacao`, `palavra`) VALUES (?, ?)";
                    PreparedStatement Query3 = c.prepareStatement(ConsultaSQL);
                    Query3.setInt(1, rs.getInt("id"));
                    Query3.setString(2, pl);
                    Query3.execute();
                }
            }

        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
    
    public List<String> getPalavras(int idtransacao) {
        List<String> palavras = new ArrayList<String>(); 
        
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`palavras` WHERE `idtransacao` = ? GROUP BY palavra ORDER BY palavra ASC";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setInt(1, idtransacao);
            rs = Query1.executeQuery();
            while (rs.next()) {
                
                palavras.add(rs.getString("palavra"));
            }
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return palavras;
    }
    
    public List<String> getAllPalavras() {
        List<String> palavras = new ArrayList<String>(); 
        
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`palavras` GROUP BY palavra ORDER BY palavra ASC";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            rs = Query1.executeQuery();
            while (rs.next()) {
                
                palavras.add(rs.getString("palavra"));
            }
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return palavras;
    }
    
    public boolean Existe() {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`transacao` WHERE `idtweet` = ?";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setString(1, idtweet);
            rs = Query1.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }

    public List<Transacao> getTransacoes() {
        List<Transacao> transacoes = new ArrayList<Transacao>();
        Connection c = ConexaoBD.getconexao();
        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`transacao`";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            rs = Query1.executeQuery();
            while (rs.next()) {
                Transacao transa = new Transacao();
                transa.setAutor(rs.getString("autor"));
                transa.setIdtweet(rs.getString("idtweet"));
                transa.setIdtransacao(rs.getInt("idtransacao"));
                transa.setPalavra(this.getPalavras(rs.getInt("idtransacao")));
                
                transacoes.add(transa);
            }
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return transacoes;
    }

    
    public List<String> getPalavra() {
        return palavra;
    }

    public void setPalavra(List<String> palavra) {
        this.palavra = palavra;
    }
    
    public String getIdtweet() {
        return idtweet;
    }

    public void setIdtweet(String idtweet) {
        this.idtweet = idtweet;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getIdtransacao() {
        return idtransacao;
    }

    public void setIdtransacao(int idtransacao) {
        this.idtransacao = idtransacao;
    }

    
}
