package classes;

import java.text.Normalizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Diorgenes
 */
public class NormalizeString {

    public String Normalizar(String str) {
        System.out.println("\n\nTratando String: "+str);
        System.out.println("\t - toUpperCase");
        str = str.toUpperCase();
        System.out.println("\t - removeLinks");
        str = removeLinks(str);
        System.out.println("\t - removeReTweet");
        str = removeReTweet(str);
        System.out.println("\t - removeEnter");
        str = removeEnter(str);
        System.out.println("\t - removeMensao");
        str = removeMensao(str);
        
        System.out.println("\t - removeAcentos");
        str = removeAcentos(str);
        System.out.println("\t - removePontuacao");
        str = removePontuacao(str);
        System.out.println("\t - removeExcessoEspaco");
        str = removeExcessoEspaco(str);
        return str;
    }
    
    public String removeEnter(String str)
    {
        str = str.replace("\n", " ");
        return str;
    }
    
    public String removeMensao(String str) {
        if(!str.contains("@")) return str;
        str = "<coisa> "+str+" <coisa>";
        System.out.println("\t\t\tºAnteMensao: "+str);

        do
        {
            
            int arroba = str.indexOf("@");
            if (str.substring(arroba, arroba + 2).equals("@ ") || str.indexOf(str.substring(arroba+1))==str.length() || str.substring(arroba, arroba + 2).equals(" @ ")) 
            {
                System.out.println("\t\tIF");
                str = str.replace("@", " ");
            }
            else
            {
                int nextSpace = str.substring(arroba, str.length()).indexOf(" ");
                
                System.out.println("\t\t\t\t-Arroba: "+arroba);
                System.out.println("\t\t\t\t-nextSpace: "+nextSpace);
                String mensao = str.substring(arroba,arroba+nextSpace);
                System.out.println("\t\t>Mensao: "+mensao);
                try
                {
                    if(str.contains(" "+mensao+" ")) str = str.replaceAll(" "+mensao+" ", " ");
                    if(str.contains("."+mensao+" ")) str = str.replaceAll("."+mensao+" ", " ");
                    if(str.contains(","+mensao+" ")) str = str.replaceAll(","+mensao+" ", " ");
                    if(str.contains("!"+mensao+" ")) str = str.replaceAll("!"+mensao+" ", " ");
                    if(str.contains("?"+mensao+" ")) str = str.replaceAll("?"+mensao+" ", " ");
                    if(str.contains("-"+mensao+" ")) str = str.replaceAll("-"+mensao+" ", " ");
                    if(str.contains("_"+mensao+" ")) str = str.replaceAll("_"+mensao+" ", " ");
                    if(str.contains("@"+mensao+" ")) str = str.replaceAll("@"+mensao+" ", " ");
                    if(str.contains("%"+mensao+" ")) str = str.replaceAll("%"+mensao+" ", " ");
                    if(str.contains("¨"+mensao+" ")) str = str.replaceAll("¨"+mensao+" ", " ");
                    if(str.contains("&"+mensao+" ")) str = str.replaceAll("&"+mensao+" ", " ");
                    if(str.contains("*"+mensao+" ")) str = str.replaceAll("*"+mensao+" ", " ");
                    if(str.contains("("+mensao+" ")) str = str.replaceAll("("+mensao+" ", " ");
                    if(str.contains(")"+mensao+" ")) str = str.replaceAll(")"+mensao+" ", " ");
                    if(str.contains("="+mensao+" ")) str = str.replaceAll("="+mensao+" ", " ");
                    if(str.contains("`"+mensao+" ")) str = str.replaceAll("`"+mensao+" ", " ");
                    if(str.contains("´"+mensao+" ")) str = str.replaceAll("´"+mensao+" ", " ");
                    if(str.contains("["+mensao+" ")) str = str.replaceAll("["+mensao+" ", " ");
                    if(str.contains("]"+mensao+" ")) str = str.replaceAll("]"+mensao+" ", " ");
                    if(str.contains("{"+mensao+" ")) str = str.replaceAll("{"+mensao+" ", " ");
                    if(str.contains("}"+mensao+" ")) str = str.replaceAll("}"+mensao+" ", " ");
                    if(str.contains(":"+mensao+" ")) str = str.replaceAll(":"+mensao+" ", " ");
                    if(str.contains("/"+mensao+" ")) str = str.replaceAll("/"+mensao+" ", " ");
                    if(str.contains("\\"+mensao+" ")) str = str.replaceAll("\\"+mensao+" ", " ");
                    if(str.contains("^"+mensao+" ")) str = str.replaceAll("^"+mensao+" ", " ");
                    if(str.contains("~"+mensao+" ")) str = str.replaceAll("~"+mensao+" ", " ");
                    if(str.contains("\""+mensao+" ")) str = str.replaceAll("\""+mensao+" ", " ");
                    if(str.contains("'"+mensao+" ")) str = str.replaceAll("'"+mensao+" ", " ");
                    str = str.replace("<coisa>"+mensao, "<coisa> ");
                    str = str.replace(mensao+"<coisa>", " <coisa>");
                }catch(Exception e)
                {
                    //Se nada mais der certo:
                    if(mensao.contains(")"))
                    {
                        mensao = mensao.replace(")", "");
                    }
                    if(str.contains(mensao)) str = str.replaceAll(mensao, " ");
                    
                    System.err.println("Caiu Catch: StrAtual: "+str);
                }
                if(mensao.contains(")"))
                {
                    mensao = mensao.replace(")", "");
                }
                
                if(str.contains(mensao)) str = str.replaceAll(mensao, " ");
                
                System.out.println("\t\t\tºPosMensao: "+str);
                str = str.replaceAll("<coisa>", "");
            }
        }while(str.contains("@"));
        
        return str;
        
    }

    public String removeReTweet(String str) {
        if ((str.length() > 4 && str.substring(0, 3).equals("RT "))) {
            int points = str.indexOf(":");
            if (points != -1) {
                str = str.substring(points + 1, str.length());
            }
        }
        return str;
    }

    public String removeLinks(String str) {
        String[] array = str.split(" ");
        String newStr = "";

        for (String string : array) {
            if ((string.length() >= 3 && string.substring(0, 3).equals("WWW"))) {
                newStr += " ";
            } else if ((string.length() >= 4 && string.substring(0, 4).equals("HTTP"))) {
                newStr += " ";
            } else {
                newStr += string + " ";
            }
        }
        try {
            return newStr.substring(0, newStr.length() - 1);
        } catch (ArrayIndexOutOfBoundsException e) {
            return "";
        }
    }

    public String removeExcessoEspaco(String str) {
        String[] array = str.split(" ");
        String newStr = "";

        for (String string : array) {
            if (!string.equals("")) {
                newStr += string + " ";
            }
        }
        if(newStr.length()>1) return newStr.substring(0, newStr.length() - 1);
        else return str;
    }

    public String removeAcentos(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;
    }

    public String removePontuacao(String str) {
        //str = str.replace("\n", " ");
        str = str.replace("0", "");
        str = str.replace("1", "");
        str = str.replace("2", "");
        str = str.replace("3", "");
        str = str.replace("4", "");
        str = str.replace("5", "");
        str = str.replace("6", "");
        str = str.replace("7", "");
        str = str.replace("8", "");
        str = str.replace("9", "");
        str = str.replace("!", "");
        str = str.replace("$", "");
        //str = str.replace("%", "");
        str = str.replace("£", "");
        str = str.replace("¢", "");
        str = str.replace("¬", "");
        str = str.replace("¨", "");
        str = str.replace("&", "");
        str = str.replace("*", "");
        str = str.replace("(", "");
        str = str.replace(")", "");
        str = str.replace("_", "");
        str = str.replace("-", "");
        str = str.replace("+", "");
        str = str.replace("=", "");
        str = str.replace("§", "");
        str = str.replace("{", "");
        str = str.replace("[", "");
        str = str.replace("ª", "");
        str = str.replace("´", "");
        str = str.replace("`", "");
        str = str.replace("}", "");
        str = str.replace("]", "");
        str = str.replace("º", "");
        str = str.replace("^", "");
        str = str.replace("~", "");
        str = str.replace("/", "");
        str = str.replace("|", "");
        str = str.replace("\\",  "");
        str = str.replace(":", "");
        str = str.replace(";", "");
        str = str.replace("°", "");
        str = str.replace("?", "");
        str = str.replace("<", "");
        str = str.replace(">", "");
        str = str.replace(".", "");
        str = str.replace(",", "");
        str = str.replace("'", "");
        str = str.replace("\"", "");
        str = str.replace("₢", "");
        return str;
    }
}
