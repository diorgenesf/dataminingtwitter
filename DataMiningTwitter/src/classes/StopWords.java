/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JTable;

/**
 *
 * @author Marcos
 */
public class StopWords {

    private int idstopwords;
    private String palavra;
    private  List<String> listStopWords = new LinkedList<String>();

    public boolean Salvar() {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";

        ConsultaSQL = "INSERT INTO `dataminingtwitter`.`stopwords` (`palavra`) VALUES (?)";
        try {
            
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setString(1, palavra);
            Query1.execute();
            
            ConsultaSQL = "DELETE FROM `dataminingtwitter`.`palavras` WHERE `palavra` = ?";
            PreparedStatement Query4 = c.prepareStatement(ConsultaSQL);
            Query4.setString(1, palavra);
            Query4.execute();

        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean Excluir() {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "DELETE FROM `dataminingtwitter`.`stopwords` WHERE  `idstopwords`= ?";
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setInt(1, idstopwords);
            Query1.execute();
            //c.commit();
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
    
    public boolean Existe() {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`stopwords` WHERE `palavra` = ?";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setString(1, palavra);
            rs = Query1.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }
    
    public boolean pesquisa() {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`stopwords` GROUP BY palavra";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            rs = Query1.executeQuery();
            while (rs.next()) {
                //palavra = rs.getString("palavra");
                listStopWords.add(rs.getString("palavra"));
            }
        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public static boolean carregaTabela(JTable Tabela) {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";

        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`stopwords`";

        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);

            rs = Query1.executeQuery();

            javax.swing.table.DefaultTableModel dtm = (javax.swing.table.DefaultTableModel) Tabela.getModel();
            int cont = dtm.getRowCount();
            for (int i = 0; i < cont; i++) {
                dtm.removeRow(0);
            }
            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getInt("idstopwords"), rs.getString("palavra")});
            }

        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean pesquisaPorPalavra(JTable Tabela) {
        Connection c = ConexaoBD.getconexao();

        String ConsultaSQL = "";
        ConsultaSQL = "SELECT * FROM `dataminingtwitter`.`stopwords` WHERE `palavra` LIKE ?";
        ResultSet rs;
        try {
            PreparedStatement Query1 = c.prepareStatement(ConsultaSQL);
            Query1.setString(1, palavra);

            rs = Query1.executeQuery();

            javax.swing.table.DefaultTableModel dtm = (javax.swing.table.DefaultTableModel) Tabela.getModel();
            int cont = dtm.getRowCount();
            for (int i = 0; i < cont; i++) {
                dtm.removeRow(0);
            }
            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getInt("idstopwords"), rs.getString("palavra")});
            }

        } catch (java.sql.SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

    public int getIdstopwords() {
        return idstopwords;
    }

    public void setIdstopwords(int idstopwords) {
        this.idstopwords = idstopwords;
    }

    public List getListStopWords() {
        return listStopWords;
    }

    public void setListStopWords(List listStopWords) {
        this.listStopWords = listStopWords;
    }

}
